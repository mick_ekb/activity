<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Active extends Model
{
    use HasFactory;
    public $timestamps = false;
    public $fillable = ['url','datetime'];

    public static function getDistinctCount(){
        $total = \DB::table('actives')->select(\DB::raw('count(distinct(url)) as count'))->first();
        return $total->count;
    }

    public static function getTrs($start, $on_page_number){
        $items=\DB::table('actives')
            ->select('url', \DB::raw('count(*) as counter,  max(`datetime`) as last_visit'))
            ->groupBy('url')
            ->offset($start)
            ->limit($on_page_number)
            ->get();
        $result=[];
        foreach($items as $item_one){
            $result[]=[
                'url'=>$item_one->url,
                'last_visit'=>$item_one->last_visit,
                'counter'=>$item_one->counter,
            ];
        }
        return $result;
    }
}
