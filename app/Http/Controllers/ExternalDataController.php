<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Active;

/**
 * Class ExternalDataController Обработка входящих запросов - модель Active зафиксирована внутри (а не сделана через DI) потому, что сам DI реализован в маршрутах на уровне выбора контроллера
 * @package App\Http\Controllers
 */
class ExternalDataController extends Controller
{
    /**
     * @param array $params Параметры визита
     * @return mixed
     * @throws \Exception
     */
    public function insertVisit(array $params)
    {
        if(!isset($params['url'])||!isset($params['time'])){
            throw new \Exception('Params error');
        }

        $active = Active::Create([
            'url' => $params['url'],
            'datetime'=> $params['time'],
        ]);

        return $active->id;
    }

    /**
     * @param array $params параметры визита - добавить страницу
     * @return array
     * @throws \Exception
     */
    public function getPage(array $params) : array
    {
        if(!is_int($params['start'])||!is_int($params['on_page_number'])){
            throw new \Exception('Params error');
        }

        $result=[
            'trs'=>Active::getTrs($params['start'],$params['on_page_number']),
            'total'=>Active::getDistinctCount(),
        ];

       return $result;
    }
}
