<?php
namespace App\Http\Response;

/**
 * Class JsonRpcResponse Форматированный ответ запроса
 * @package App\Http\Response
 */
class JsonRpcResponse
{
    const JSON_RPC_VERSION = '2.0';

    /**
     * @param $result Результат - массив или число
     * @param string|null $id Ид записи, если есть
     * @return array
     */
    public static function success($result, string $id = null)
    {
        return [
            'jsonrpc' => self::JSON_RPC_VERSION,
            'result'  => $result,
            'id'      => $id,
        ];
    }

    /**
     * @param $error сообщение об ошибке
     * @return array
     */
    public static function error($error)
    {
        return [
            'jsonrpc' => self::JSON_RPC_VERSION,
            'error'  => $error,
            'id'      => null,
        ];
    }
}
