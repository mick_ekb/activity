<?php
namespace App\Services;
use App\Http\Response\JsonRpcResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class JsonRpcServer Сервер - json, принимает соединения
 * @package App\Services
 */
class JsonRpcServer
{
    /**
     * @param Request $request - запрос
     * @param Controller $controller - контроллер для маршрутизации
     * @return array
     */
    public function handle(Request $request, Controller $controller)
    {
        try {
            $content = json_decode($request->getContent(), true);

            if (empty($content)&&isset($content['method'])) {
                throw new \Exception('Parse error');
            }

            $result = $controller->{$content['method']}(...[$content['params']]);

            return JsonRpcResponse::success($result);
        }
        catch (\Exception $e) {
            return JsonRpcResponse::error($e->getMessage());
        }
    }
}
